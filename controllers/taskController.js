//Controllers contain the functions and business logic of our Express JS application
 
// Uses the "require" directive to allow access to the "Task" model which allows us to access Mongoose methods to perform CRUD functions
//Allow us to use the contents of the "Task.js" file in the models folder
const Task = require("../models/Task");

//Controller function for getting all the tasks
// Defines the functions to be used in the "taskRoute.js" file and exports these functions
module.exports.getAllTasks = () => {


	return Task.find({}).then(result => {

		return result
	})

}


module.exports.createTask = (requestBody) => {


	let newTask = new Task ({
		
		name: requestBody.name

	});

	return newTask.save().then((task, error) => {

		if (error) {
			console.log(error)
			return false
		} else {
			return task
		}
	});
};


module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndRemove(taskId).then((removedTask, err) =>{

		if(err){
			console.log(err);
			return false
		} else {
			return removedTask
		}
	})

}

// Updating Tasks
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if (error) {
			console.log(error);
			return false
		}

		result.name = newContent.name;

		return result.save().then((updateTask, saveErr) => {
			if (saveErr) {
				console.log(saveErr);
				return false
			} else {
				return updateTask;
			}
		})

	})
}