//Contains all the endpoints for our application
//We separate the routes such that "index.js" only contains information on the server
// We need to use express' Router() function to achieve this
const express = require("express");
// Creates a Router instance that functions as a middleware and routing system
// Allows access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router();

const taskController = require("../controllers/taskController");
 

//Routes
//Route to get all the tasks

router.get("/", (req, res) => {

	// Invokes the "getAllTasks" function from the "taskController.js" file and sends the result back to the client/Postman
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))

});

// Route to post a task
router.post("/", (req, res) => {
	console.log(req.body)

	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
})

router.delete('/:id', (req, res) => {

	console.log(req.params)
	// 633d7
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))

});

// Route to update a task
router.put("/:id", (req, res) => {
	console.log(req.params.id);
	console.log(req.body);

	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});


//Use "module.exports" to export the router object to use in the "index.js"
module.exports = router;